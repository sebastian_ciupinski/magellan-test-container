FROM ubuntu:latest
SHELL ["/bin/bash", "-c"]
RUN apt-get update; apt-get clean;
#RUN pip install virtualenv virtualenvwrapper
RUN apt-get install -y sudo
# Add a user for running applications.
RUN useradd robot
RUN mkdir -p /home/robot && chown robot:robot /home/robot
RUN echo "robot ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# Install x11vnc.
#RUN apt-get install -y x11vnc curl bsdmainutils psmisc libnss3-tools

# Install xvfb.
RUN apt-get install -y xvfb

# Install fluxbox.
#RUN apt-get install -y fluxbox

# Install wget.
RUN apt-get install -y wget

# Install wmctrl.
RUN apt-get install -y wmctrl

# Install python
RUN apt-get update
RUN apt-get install python3 python3-pip curl libnss3-tools -y
#RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py;
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 10
RUN sudo pip3 install virtualenv virtualenvwrapper
# Set the Chrome repo.
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list

# Install Chrome.
RUN apt-get update && apt-get -y install google-chrome-stable

# Install git

RUN apt-get install -y git


COPY setup/bootstrap.sh /
RUN chmod 777 /bootstrap.sh

RUN mkdir -p /etc/opt/chrome/policies/managed/

USER robot
COPY bin /test/bin
#COPY libraries /test/libraries
#COPY resources /test/resources
COPY setup /test/setup
COPY test /test/test
RUN sudo chmod -R 777 /test
RUN echo "source $(which virtualenvwrapper.sh)" >> /home/robot/.bashrc
RUN sudo apt-get update; sudo apt-get upgrade -y


RUN /test/setup/full-setup robot
RUN sudo ln -s /test/bin/robot /usr/local/bin/robot
RUN sudo mkdir /input; sudo chmod 777 /input
RUN sudo mkdir /output; sudo chmod 777 /output
RUN sudo mkdir /upload; sudo chmod 777 /upload
